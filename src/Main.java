import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the countApplesAndOranges function below.
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int applesOnHouse=0, orangesOnHouse=0;
        for(int i=0;i<apples.length;i++){
            apples[i]=apples[i]+a;
        }

        for(int i=0;i<oranges.length;i++){
            oranges[i]=oranges[i]+b;
        }

        for (int i=0;i<apples.length;i++){
            if(apples[i]>=s && apples[i]<=t){
                applesOnHouse++;
            }
        }
        for (int i=0;i<oranges.length;i++){
            if(oranges[i]>=s && oranges[i]<=t){
                orangesOnHouse++;
            }
        }
        System.out.println(applesOnHouse);
        System.out.println(orangesOnHouse);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int s = 7;

        int t = 11;


        int a = 5;

        int b = 15;



        int[] apples = {-2,2,1};


        int[] oranges = {5,-6};


        countApplesAndOranges(s, t, a, b, apples, oranges);

    }
}
